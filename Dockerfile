FROM ubuntu:18.04
RUN cp /bin/cat /bin/supercat && chmod 4755 /bin/supercat
RUN useradd -ms /bin/bash luser
USER luser
CMD ["sh","-c", "/bin/cat /etc/shadow; /bin/supercat /etc/shadow"]
